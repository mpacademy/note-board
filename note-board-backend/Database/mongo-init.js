db.createUser(
  {
    user: "alice",
    pwd: "password",
    roles: [
      {
        role: "readWrite",
        db: "Notes"
      }
    ]
  }
);

db.createCollection('Notes');