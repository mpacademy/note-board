using NoteBoardApi.Models;
using NoteBoardApi.Services;

var MyAllowSpecificOrigins = "MyPolicy";
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
        policy => {
            // TODO: add origin control
            policy
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});
builder.Services.AddDatabaseDeveloperPageExceptionFilter();
builder.Services.Configure<NoteBoardDatabaseSettings>(
    builder.Configuration.GetSection("NoteBoardDatabase"));
builder.Services.AddSingleton<NotesService>();
builder.Services.AddControllers();

var app = builder.Build();

if (app.Environment.IsDevelopment()) {
    app.UseDeveloperExceptionPage();
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseCors();
app.UseAuthorization();
app.MapControllers().RequireCors(MyAllowSpecificOrigins);

app.Run();