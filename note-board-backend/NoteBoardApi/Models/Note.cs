using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NoteBoardApi.Models;

public class Note
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    public string Title { get; set; } = null!;

    public string Body { get; set; } = null!;
}