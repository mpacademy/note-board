namespace NoteBoardApi.Models;

public class NoteBoardDatabaseSettings
{
    public string Host { get; set; } = null!;
    public int Port { get; set; }
    public string User { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string ConnectionString 
        {
            get 
            {
                if (string.IsNullOrEmpty(User) || string.IsNullOrEmpty(Password))
                    return $@"mongodb://{Host}:{Port}";
                return $@"mongodb://{User}:{Password}@{Host}:{Port}";
            }
        }
    public string DatabaseName { get; set; } = null!;
    public string NotesCollectionName { get; set; } = null!;
}