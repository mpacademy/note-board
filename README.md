# Note Board App

## Open the **[Demo App](http://a77617dfe1ba148879a1f6e6485b265b-278179804.us-east-1.elb.amazonaws.com)**

## Local development

Clone the repo and run the Stack with 

`docker compose up -d` 

inside the root folder.

The Stack exposes the following UIs:
- MongoExpress: `localhost:8081` 
- Angular App: `localhost:4200`
