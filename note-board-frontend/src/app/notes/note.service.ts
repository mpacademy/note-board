import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from './note';
import { environment as env } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(private http: HttpClient) { }

  getHeaders() {
    return { headers: {'Content-Type': 'application/json'} }
  }

  getAll(): Observable<Note[]> {
    return this.http.get<Note[]>(`${env.apiUrl}/api/notes`, this.getHeaders())
  }

  get(id: string): Observable<Note> {
    return this.http.get<Note>(`${env.apiUrl}/api/notes/` + id, this.getHeaders());
  }

  add(note: Note): Observable<Note> {
    return this.http.post<Note>(`${env.apiUrl}/api/notes`, note, this.getHeaders());
  }

  update(note: Note): Observable<Note> {
    return this.http.put<Note>(`${env.apiUrl}/api/notes/` + note.id, note, this.getHeaders());
  }

  delete(id: string) {
    return this.http.delete(`${env.apiUrl}/api/notes/` + id);
  }
}
