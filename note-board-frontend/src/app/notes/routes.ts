import { NoteDetailComponent } from "./note-detail/note-detail.component";
import { NoteOverviewComponent } from "./note-overview/note-overview.component";
import { NoteService } from "./note.service";

export default [
  {path: '', providers: [NoteService], children: [
    {path: '', component: NoteOverviewComponent},
    {path: 'create', component: NoteDetailComponent},
    {path: ':id/edit', component: NoteDetailComponent},
  ]}
];