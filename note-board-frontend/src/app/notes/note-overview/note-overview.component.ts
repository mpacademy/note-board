import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteService } from '../note.service';
import { Note } from '../note';
import { MatGridListModule } from '@angular/material/grid-list';
import { trigger, transition, style, animate, query, stagger} from '@angular/animations';
import { NoteCardComponent } from '../note-card/note-card.component';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-note-overview',
  standalone: true,
  imports: [
    CommonModule, 
    MatGridListModule, 
    MatButtonModule, 
    NoteCardComponent,
    RouterModule
  ],
  templateUrl: './note-overview.component.html',
  styleUrl: './note-overview.component.scss',
  animations: [
    trigger('itemAnim', [
      // init animation
      transition('void => *', [
        style({
          height: 0,
          opacity: 0,
          transform: 'scale(0.85)',
          'margin-bottom': 0,
          paddingTop: 0,
          paddingBottom: 0,
          paddingLeft: 0,
          paddingRight: 0,
        }),
        animate('50ms', style({
          height: '*',
          'margin-bottom': '*',
          paddingTop: '*',
          paddingBottom: '*',
          paddingLeft: '*',
          paddingRight: '*',
        })),
        animate(68)
      ]),

      transition('* => void', [
        // initial scale up
        animate(50, style({
          transform: 'scale(1.05)'
        })),
        animate(50, style({
          transform: 'scale(1)',
          opacity: 0.75
        })),
        animate('120ms ease-out', style({
          transform: 'scale(0.68)',
          opacity: 0,
        })),
        animate('150ms ease-out', style({
          height: 0,
          paddingTop: 0,
          paddingBottom: 0,
          paddingRight: 0,
          paddingLeft: 0,
          'margin-bottom': '0',
        }))
      ])
    ]),

    trigger('listAnim', [
      transition('* => *', [
        query(':enter', [
          style({
            opacity: 0,
            height: 0
          }),
          stagger(100, [
            animate('0.2s ease')
          ])
        ], {
          optional: true
        })
      ])
    ])
  ]
})
export class NoteOverviewComponent implements OnInit {

  notes: Note[] = []

  constructor(private noteService: NoteService) { }
  
  ngOnInit(): void {
    this.noteService.getAll().subscribe({
      next: notes => this.notes = notes, 
      error: err => console.error(err)
    })
  }

  deleteNote(note: Note) {
    this.noteService.delete(note.id).subscribe(() => {
      // remove the note from the notes array
      this.notes.splice(this.notes.indexOf(note), 1);
    }) 
  }

}
