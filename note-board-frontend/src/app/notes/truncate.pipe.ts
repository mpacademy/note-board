import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate',
  standalone: true
})
export class TruncatePipe implements PipeTransform {

  constructor() { }

  transform(value: string, limit: number): string {
    return value.split(' ').length < limit
      ? value
      : value.split(' ').slice(0, limit).join(' ') + '...';
  }
}
