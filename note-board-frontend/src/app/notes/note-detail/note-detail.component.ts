import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteService } from '../note.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Note } from '../note';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-note-detail',
  standalone: true,
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule, 
    ReactiveFormsModule,
    MatButtonModule
  ],
  templateUrl: './note-detail.component.html',
  styleUrl: './note-detail.component.scss'
})
export class NoteDetailComponent {
  note?: Note;
  form: FormGroup;
  
  constructor(
    private notesService: NoteService, 
    private router: Router, 
    private route: ActivatedRoute,
    private fb: FormBuilder) { 
      this.form = this.fb.group({
        title: ['', Validators.required],
        body: ['', Validators.required]
      })
    }

  get title() {
    return this.form.get('title');
  }

  get body() {
    return this.form.get('body');
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        this.notesService.get(params['id']).subscribe((note: Note) => {
          this.note = note;
          const {id, ...rest} = note;
          this.form.patchValue(rest);
        });
      }
    })
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    const value = this.form.value;
    if (!this.note) {
      this.notesService.add(value).subscribe(() => {
        this.router.navigateByUrl('/');
      })
    } else {
      const toUpdate = {...this.note, ...value};
      this.notesService.update(toUpdate).subscribe(() => {
        this.router.navigateByUrl('/');
      })
    }
  }

  cancel() {
    this.router.navigateByUrl('/');
  }
}
